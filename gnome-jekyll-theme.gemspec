# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "gnome-jekyll-theme"
  spec.version       = "0.1.0"
  spec.authors       = ["Çağatay Yiğit Şahin"]
  spec.email         = ["cyigitsahin@outlook.com"]

  spec.summary       = "GNOME Deneb Jekyll theme"
  spec.homepage      = "https://gitlab.gnome.org/cagatay-y/gnome-jekyll-theme"
  spec.license       = "ISC"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.7"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
