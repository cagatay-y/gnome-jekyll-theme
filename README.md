# GNOME Deneb

Bootstrap theme for GNOME websites

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "gnome-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: gnome-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gnome-jekyll-theme

## Get started
Requirements:
Jekyll package

#### Running the project:
* Clone the repo
 
 ```
 git clone https://gitlab.gnome.org/cagatay-y/gnome-jekyll-theme.git
 ```
* Install Ruby dependencies
 
 ```
 bundle install
 ```
* Run the project
 
 ```
 jekyll serve
 ```
 Visit local webserver http://localhost:4000/ to see or edit the created theme.
